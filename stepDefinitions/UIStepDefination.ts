import {EJViewerPageObjects} from '../pages/ejviewerPageObjects';
import {LogInPageObjects} from '../pages/logInPageObjects';

const { Given,  Then, When } = require('cucumber');


Given(/^open my url$/, async () => {
  await  browser.url('https://apps.test.aptos-denim.aptos-labs.io/');

 });

Given(/^User launch APTOS "([^"]*)" and navigated to APTOS Login Screen$/, async (URL) => {
    browser.url(URL);
    console.log('APTOS Home Screen is displayed successfully');
    await browser.pause(5000);
    LogInPageObjects.loginLink.click();
    console.log('Able to launch the Login screen successfully');
    await browser.pause(5000);
});

When(/^User enter "([^"]*)" and "([^"]*)"$/, async (userName, password) => {
    // await browser.pause(5000);
    LogInPageObjects.userNameTextbox.setValue(userName);
    LogInPageObjects.passwordTextbox.setValue(password);
    LogInPageObjects.logInButton.click();
    await browser.pause(5000);
});

Then(/^User navigate to EJViewer and select "([^"]*)" and Enter "([^"]*)"$/, async (store , deviceId) => {
    await browser.pause(5000);
    EJViewerPageObjects.showFilterButton.click();
    await browser.pause(5000);
    await EJViewerPageObjects.storeList.click();
    await EJViewerPageObjects.selectStore.click();
    await browser.pause(5000);
    console.log('selected ' + store + ' from the store list');
    // browser.element('css=.btn > span').click();
    console.log('clicked%%%%%%%%%%%%%%%%%');
    await browser.pause(3000);
    EJViewerPageObjects.deviceIDTextbox.setValue(deviceId);
    await browser.pause(5000);
});
