import {RestUtils} from '../../utils/restUtils';
require('../../hooks/restapihooks.ts');
const {Given , Then} = require('cucumber');
require('../../hooks/restapihooks.ts');

const RestUtilsobj: RestUtils = new RestUtils();
let response;

Given(/^send GET request to "([^"]*)"$/, async (url) => {
    response = await RestUtilsobj.getRequestWithAccessToken(url, RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response.body);
});

Then(/^user response includes the following$/, async (expectedresponse) => {
    RestUtilsobj.validateResponse(await RestUtilsobj.responseData, expectedresponse);

});

Then(/^response includes the following$/, async ( responseFields ) => {
    RestUtilsobj.validateJsonPathWithRegularExpression(await RestUtilsobj.responseData, responseFields);

});

Then(/^response not includes the following$/, async ( responseFields ) => {
    // RestUtilsobj.validateResponseWithJsonPath(await RestUtilsobj.responseData, responseFields);
    RestUtilsobj.invalidJsonPathWithRegularExpression(await RestUtilsobj.responseData, responseFields);
});
