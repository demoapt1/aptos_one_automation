/**
 * WebdriverIO config file to run tests on native mobile apps.
 * Config file helps us configure all the settings and setup environments 
 * to run our tests.
 */
//const report = require('multiple-cucumber-html-reporter');

const host = '127.0.0.1';   // default appium host
const port = 4723;          // default appium port

const waitforTimeout = 30 * 60000;
const commandTimeout = 30 * 60000;

exports.config = {
    debug: false,
    specs: [
        './features/iosapp.feature',
    ],

    reporters: ['multiple-cucumber-html'],
    reporterOptions: {
        htmlReporter: {
            removeFolders: true,
            jsonFolder: './report',
            reportFolder: './report/ios/',
            // ... other options, see Options
            displayDuration: true,
            openReportInBrowser: true,
            saveCollectedJSON: true,
            disableLog: true,
            pageTitle: 'APTOS_IOS_AUTOMATION_REPORT',
            reportName: 'APTOS_IOS_AUTOMATION_REPORT',
            pageFooter: '<div><h1>APTOS</h1></div>',
            customData: false,
            customData: {
                title: 'IOS Mobile Automation Run info',
                data: [
                    {label: 'Project', value: 'IOS_MOBILE_AUTOMATION'},
                    {label: 'Release', value: '1.2.3'},
                    {label: 'Cycle', value: 'B11221.34321'},
                    {label: 'Execution Start Time', value: 'Nov 19th 2017, 02:31 PM EST'},
                    {label: 'Execution End Time', value: 'Nov 19th 2017, 02:56 PM EST'}
                ]
            },
        }
    },

    host: host,
    port: port,

    maxInstances: 1,

    capabilities: [
        {

            newCommandTimeout: 30 * 60000,
            deviceName: 'iPhone 6',
            platformName: 'iOS',
            platformVersion: '12.1',
            orientation: 'PORTRAIT',
            // The path to the app
            
            app:  './app/wdioDemoApp.app',
            // Read the reset strategies very well, they differ per platform, see
            // http://appium.io/docs/en/writing-running-appium/other/reset-strategies/
            noReset: true,
            newCommandTimeout: 240,
        }
    ],

    services: ['appium'],
    appium: {
        waitStartTime: 6000,
        waitforTimeout: waitforTimeout,
        command: 'appium',
        logFileName: 'appium.log',
        args: {
            address: host,
            port: port,
            commandTimeout: commandTimeout,
            sessionOverride: true,
            debugLogSpacing: true
        }
    },

    /**
     * test configurations
     */
    logLevel: 'silent',
    coloredLogs: true,
    framework: 'cucumber',          // cucumber framework specified 
    cucumberOpts: {
        compiler: ['ts:ts-node/register'],
        backtrace: true,
        failFast: false,
        timeout: 5 * 60 * 60000,
        require: ['./stepDefinitions/iosapp.ts']      // importing/requiring step definition files
    },

    /**
     * hooks help us execute the repeatitive and common utilities 
     * of the project.
     */
    onPrepare: function () {
        console.log('<<< NATIVE APP TESTS STARTED >>>');

    },
    
    afterScenario: function (scenario) {
       // browser.screenshot();
     },


    onComplete: function () {
        console.log('<<< TESTING FINISHED >>>');
    },

};
