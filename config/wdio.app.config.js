/**
 * WebdriverIO config file to run tests on native mobile apps.
 * Config file helps us configure all the settings and setup environments 
 * to run our tests.
 */

const host = '127.0.0.1';   // default appium host
//const port = 4723;          // default appium port
const port = 4725;
const waitforTimeout = 30 * 60000;
const commandTimeout = 30 * 60000;

exports.config = {
    debug: false,
    specs: [
        './features/calculator.feature',
    ],

    reporters: ['multiple-cucumber-html'],
    reporterOptions: {
        htmlReporter: {
            removeFolders: true,
            jsonFolder: './report',
            reportFolder: './report/android',
            // ... other options, see Options
            displayDuration: true,
            openReportInBrowser: true,
            saveCollectedJSON: true,
            disableLog: true,
            pageTitle: 'APTOS_ANDROID_AUTOMATION',
            reportName: 'aptos_android_automation',
            pageFooter: '<div><h1>APTOS_ANDROID_AUTOMATION</h1></div>',
            customData: false,
            customData: {
                title: 'ANDROID Run info',
                data: [
                    {label: 'Project', value: 'APTOS_ONE'},
                    {label: 'Release', value: '1.2.3'},
                    {label: 'Cycle', value: 'B11221.34321'},
                    {label: 'Execution Start Time', value: 'Nov 19th 2017, 02:31 PM EST'},
                    {label: 'Execution End Time', value: 'Nov 19th 2017, 02:56 PM EST'}
                ]
            },
        }
    },



    host: host,
    port: port,

    maxInstances: 1,

    capabilities: [
        {
            appiumVersion: '1.10.0',                 // Appium module version
            browserName: '',                        // browser name is empty for native apps
            platformName: 'Android',
            app: './app/apkfiles.com_427875_Pen-It.apk',          // Path to your native app
            appPackage: 'com.penit.rob',  // Package name of your app
            appActivity: 'com.penit.rob.InfoActivity', // App activity of the app
            platformVersion: '7.1.1',              // Android platform version of the device
            deviceName: 'emulator-5554',              // device name of the mobile device
            waitforTimeout: waitforTimeout,
            commandTimeout: commandTimeout,
            newCommandTimeout: 30 * 60000,
        }
    ],

    services: ['appium'],
    appium: {
        waitStartTime: 6000,
        waitforTimeout: waitforTimeout,
        command: 'appium',
        logFileName: 'appium.log',
        args: {
            address: host,
            port: port,
            commandTimeout: commandTimeout,
            sessionOverride: true,
            debugLogSpacing: true
        }
    },

    /**
     * test configurations
     */
    logLevel: 'silent',
    coloredLogs: true,
    framework: 'cucumber',          // cucumber framework specified 
    cucumberOpts: {
        compiler: ['ts:ts-node/register'],
        backtrace: true,
        failFast: false,
        timeout: 5 * 60 * 60000,
        require: ['./stepDefinitions/calcSteps.ts']      // importing/requiring step definition files
    },

    /**
     * hooks help us execute the repeatitive and common utilities 
     * of the project.
     */
    onPrepare: function () {
        console.log('<<< NATIVE APP TESTS STARTED >>>');
    },

    afterScenario: function (scenario) {
        browser.screenshot();
     },

    onComplete: function () {
        console.log('<<< TESTING FINISHED >>>');
    }

};
