
export class SearchPageObject {
    public get searchTextBox()  { return browser.element('//input[title=\'Search\']'); }
    public get logo()  { return browser.element('#logo > img'); }
    public get searchButton()  { return browser.element("//input[value='Google Search']"); }
    public open(): void {
        browser.url('https://www.google.com');
    }
}
const SearchPageObj = new SearchPageObject();
export default SearchPageObj;
