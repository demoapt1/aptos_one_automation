"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testrailUtil_1 = require("../reporthelper/testrailUtil");
const { Before, BeforeAll, After, AfterAll, Status } = require('cucumber');
const testrail = new testrailUtil_1.TestRailreport();
BeforeAll({ timeout: 100 * 1000 }, async () => {
    console.log('Before ALL');
});
After(async function (scenario) {
    console.log('SCEnario ' + scenario.result.status.toString());
    const caseids = scenario.pickle.name.split(' ');
    console.log('CASE IDS --------------' + caseids[0].charAt(1));
    const content = new testrailUtil_1.IContentObjectImpl();
    content.assignedto_id = '1';
    content.comment = 'update result';
    content.version = '100';
    content.defects = 'APT458';
    if (scenario.result.status === Status.FAILED) {
        content.status_id = 5;
        console.log('STATUS B ' + content.status_id);
    }
    else if (scenario.result.status === Status.PASSED) {
        content.status_id = 1;
        console.log('STATUS B ' + content.status_id);
    }
    await testrail.addTestResult(4, caseids[0].charAt(1), content);
    /* console.log( 'Entries         ' +scenario.pickle.steps.entries());
     scenario.pickle.steps.forEach(function (value) {
         Object.keys(value).forEach(function(prop) {
             console.log(prop + " = " + value[prop]);
             const valueprop = value[prop];
             if ( valueprop instanceof  Object == true) {
                 Object.keys(valueprop).forEach(function(subvalue) {
                     console.log(subvalue + '=' + valueprop[subvalue] );
                 });
             }
         });*/
});
Before(async function (scenario) {
    console.log('SCENARIO NAME' + scenario.pickle.name);
    /*   console.log('  BEFORE steps VALUES' + scenario.pickle.steps.values().);
       scenario.pickle.steps.values().forEach(function (value) {
   
               console.log('  BEFORE steps ' + value);
   
       });*/
});
AfterAll({ timeout: 100 * 1000 }, async () => {
    //   await browser.quit();
});
Before(async function () {
    // This hook will be executed before all scenarios
});
Before({ tags: '@foo' }, function () {
    // This hook will be executed before scenarios tagged with @foo
});
Before({ tags: '@foo and @bar' }, function () {
    // This hook will be executed before scenarios tagged with @foo and @bar
});
Before({ tags: '@foo or @bar' }, function () {
    // This hook will be executed before scenarios tagged with @foo or @bar
});
// You can use the following shorthand when only specifying tags
Before('@foo', function () {
    // This hook will be executed before scenarios tagged with @foo
});
/*

setDefinitionFunctionWrapper ( function (fn) {

    if (isGenerator.fn(fn)) {
         p.coroutine(fn);
    } else {
        ;
    }
});*/
//# sourceMappingURL=restapihooks.js.map