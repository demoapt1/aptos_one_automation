"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ejviewerPageObjects_1 = require("../pages/ejviewerPageObjects");
const logInPageObjects_1 = require("../pages/logInPageObjects");
const { Given, Then, When } = require('cucumber');
Given(/^open my url$/, async () => {
    await browser.url('https://apps.test.aptos-denim.aptos-labs.io/');
});
Given(/^User launch APTOS "([^"]*)" and navigated to APTOS Login Screen$/, async (URL) => {
    browser.url(URL);
    console.log('APTOS Home Screen is displayed successfully');
    await browser.pause(5000);
    logInPageObjects_1.LogInPageObjects.loginLink.click();
    console.log('Able to launch the Login screen successfully');
    await browser.pause(5000);
});
When(/^User enter "([^"]*)" and "([^"]*)"$/, async (userName, password) => {
    // await browser.pause(5000);
    logInPageObjects_1.LogInPageObjects.userNameTextbox.setValue(userName);
    logInPageObjects_1.LogInPageObjects.passwordTextbox.setValue(password);
    logInPageObjects_1.LogInPageObjects.logInButton.click();
    await browser.pause(5000);
});
Then(/^User navigate to EJViewer and select "([^"]*)" and Enter "([^"]*)"$/, async (store, deviceId) => {
    await browser.pause(5000);
    ejviewerPageObjects_1.EJViewerPageObjects.showFilterButton.click();
    await browser.pause(5000);
    await ejviewerPageObjects_1.EJViewerPageObjects.storeList.click();
    await ejviewerPageObjects_1.EJViewerPageObjects.selectStore.click();
    await browser.pause(5000);
    console.log('selected ' + store + ' from the store list');
    // browser.element('css=.btn > span').click();
    console.log('clicked%%%%%%%%%%%%%%%%%');
    await browser.pause(3000);
    ejviewerPageObjects_1.EJViewerPageObjects.deviceIDTextbox.setValue(deviceId);
    await browser.pause(5000);
});
//# sourceMappingURL=UIStepDefination.js.map