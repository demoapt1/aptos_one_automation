"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restUtils_1 = require("../../utils/restUtils");
require('../../hooks/restapihooks.ts');
const { Given, Then } = require('cucumber');
require('../../hooks/restapihooks.ts');
const RestUtilsobj = new restUtils_1.RestUtils();
let response;
Given(/^send GET request to "([^"]*)"$/, async (url) => {
    response = await RestUtilsobj.getRequestWithAccessToken(url, restUtils_1.RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response.body);
});
Then(/^user response includes the following$/, async (expectedresponse) => {
    RestUtilsobj.validateResponse(await RestUtilsobj.responseData, expectedresponse);
});
Then(/^response includes the following$/, async (responseFields) => {
    RestUtilsobj.validateJsonPathWithRegularExpression(await RestUtilsobj.responseData, responseFields);
});
Then(/^response not includes the following$/, async (responseFields) => {
    // RestUtilsobj.validateResponseWithJsonPath(await RestUtilsobj.responseData, responseFields);
    RestUtilsobj.invalidJsonPathWithRegularExpression(await RestUtilsobj.responseData, responseFields);
});
//# sourceMappingURL=getUserListSteps.js.map