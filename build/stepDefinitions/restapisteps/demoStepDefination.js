"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restUtils_1 = require("../../utils/restUtils");
const serviceClient_1 = require("../../utils/serviceClient");
const assert = require('assert');
const { Given, Then, When } = require('cucumber');
const client = new serviceClient_1.serviceClient();
let randomString = require('randomstring');
/*
Given(/^send GET request to "([^"]*)"$/, async (url) => {
    response = await RestUtilsobj.getRequestWithAccessToken(url, RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response.body);
});

Given(/^send DELETE request to "([^"]*)"$/, async (url) => {
    response = await RestUtilsobj.deleteRequestWithAccessToken(url, RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response.body);
});
*/
/**
 *To Get access Token
 */
Given(/^get access token from "([^"]*)", input body is$/, async (url, inputbody) => {
    await client.getAccessToken(url, inputbody);
});
/**
 * For Create User
 */
When(/^I create a user in the application by using input as$/, async (request) => {
    let randomUserName = randomString.generate({
        length: 5,
        charset: 'numeric'
    });
    let body = request.replace("user_name", randomUserName);
    await client.createUser(body);
});
/**
 * To check Status code
 */
Then(/^the status code will be (.*)$/, async (statusCode) => {
    assert.strictEqual(restUtils_1.RestUtils.getStatusCode(), statusCode.toString(), statusCode +
        'Status code is not equal' + restUtils_1.RestUtils.getStatusCode());
});
/**
 * To get user List
 */
When(/^I get users list$/, async () => {
    await client.getUserList();
});
//# sourceMappingURL=demoStepDefination.js.map