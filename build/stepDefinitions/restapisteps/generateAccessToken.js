"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restUtils_1 = require("../../utils/restUtils");
require('../../hooks/restapihooks.ts');
const assert = require('assert');
const { Given, Then } = require('cucumber');
const RestUtilsobj = new restUtils_1.RestUtils();
let response;
Given(/^send POST request to "([^"]*)", input body is$/, async (url, inputbody) => {
    response = await RestUtilsobj.postformbody(url, inputbody);
    console.log('STATUS CODE' + response.statusCode);
    const json = JSON.parse(response.body);
    restUtils_1.RestUtils.setAccessToken(json.access_token);
    console.log('ACCESS TOKEN' + restUtils_1.RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response);
    restUtils_1.RestUtils.setStatusCode(response.statusCode);
});
Then(/^the status code must be (.*)$/, async (statusCode) => {
    assert.strictEqual(restUtils_1.RestUtils.getStatusCode().toString(), statusCode.toString(), statusCode +
        'Status code is not equal' + restUtils_1.RestUtils.getStatusCode());
});
//# sourceMappingURL=generateAccessToken.js.map