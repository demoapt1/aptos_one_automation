"use strict";
/**
 * Page Objects help in better re-usablitity and maintenance of element locators.
 * This file exports CalculatorPageObject class
 */
Object.defineProperty(exports, "__esModule", { value: true });
class CalculatorPageObject {
    constructor() {
        this.outputText = 'com.android.calculator2.CalculatorEditText';
        this.idLocator = 'com.android.calculator2:id/';
        // public digitLocator: string = 'com.android.calculator2:id/digit';
        this.digitLocator = 'com.penit.rob:id/';
        this.idElementLocator = 'com.penit.rob:id/';
        this.exit = 'info_done';
        this.calcDigitSelector = (selector) => {
            return this.androidIDSelector(this.digitLocator + selector);
        };
        this.androidIDSelector = (selector) => {
            let str = `'android=new UiSelector().resourceId("${selector}")'`;
            str = str.substring(1, str.length - 1);
            return str;
        };
        this.androidClassSelector = (selector) => {
            let str = `'android=new UiSelector().className("${selector}")'`;
            str = str.substring(1, str.length - 1);
            return str;
        };
        this.calcOperatorSelector = (selector) => {
            return this.idLocator + selector;
        };
        this.penIdSelector = (selector) => {
            let str = `'android=new UiSelector().resourceId("${selector}")'`;
            str = str.substring(1, str.length - 1);
            return str;
        };
        this.addOperator = this.androidIDSelector(this.calcOperatorSelector('plus'));
        this.subtractOperator = this.androidIDSelector(this.calcOperatorSelector('minus'));
        this.multiplyOperator = this.androidIDSelector(this.calcOperatorSelector('mul'));
        this.divisionOperator = this.androidIDSelector(this.calcOperatorSelector('div'));
        this.equalOperator = this.androidIDSelector(this.calcOperatorSelector('equal'));
        this.clearOperator = this.androidIDSelector(this.calcOperatorSelector('allClear'));
        this.outputText = this.androidClassSelector(this.outputText);
        this.exit = this.penIdSelector(this.calcOperatorSelector('info_done'));
    }
}
exports.CalculatorPageObject = CalculatorPageObject;
//# sourceMappingURL=calcPage.js.map