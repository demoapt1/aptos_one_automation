"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LogInPageObjects {
    static get loginLink() {
        return browser.element("//span[contains(.,'Log In')]");
    }
    // entering user Name
    static get userNameTextbox() {
        return browser.element("//*[@id='username']");
    }
    // Entering pass word
    static get passwordTextbox() {
        return browser.element("//*[@id='password']");
    }
    // clicks on login button
    static get logInButton() {
        return browser.element("//input[@name='login']");
    }
}
exports.LogInPageObjects = LogInPageObjects;
//# sourceMappingURL=logInPageObjects.js.map