"use strict";
/**
 * Page Objects help in better re-usablitity and maintenance of element locators.
 * This file exports GooglePageObject & AppiumPageObject classes
 */
Object.defineProperty(exports, "__esModule", { value: true });
class GooglePageObject {
    get searchTextBox() { return browser.element('#lst-ib'); }
    get searchButton() { return browser.element('button[name="btnG"]'); }
    get results() { return browser.waitForVisible('#rso', 5000); }
    get firstLink() { return browser.element('#rso div._H1m._ees'); }
}
class AppiumPageObject {
    get linkButton() { return browser.element('body > nav.navbar.navbar-inverse.navbar-static-top button'); }
    get tutorialLink() { return browser.element('#bs-example-navbar-collapse-1 > ul > li:nth-child(7) > a'); }
    get androidLink() { return browser.element('#readmeMarkdown > div > a:nth-child(3)'); }
    get androidTutorialTitle() { return browser.element('#native-android-automation').getText(); }
}
/*
Public Interface - export instances of classes
**/
exports.GooglePage = new GooglePageObject();
exports.AppiumPage = new AppiumPageObject();
//# sourceMappingURL=appiumPage.js.map