"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SearchPageObject {
    get searchTextBox() { return browser.element('//input[title=\'Search\']'); }
    get logo() { return browser.element('#logo > img'); }
    get searchButton() { return browser.element("//input[value='Google Search']"); }
    open() {
        browser.url('https://www.google.com');
    }
}
exports.SearchPageObject = SearchPageObject;
const SearchPageObj = new SearchPageObject();
exports.default = SearchPageObj;
//# sourceMappingURL=searchPage.js.map