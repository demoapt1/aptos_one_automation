"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restUtils_1 = require("./restUtils");
const RestUtilsobj = new restUtils_1.RestUtils();
let PropertiesReader = require('properties-reader');
let response;
let properties = PropertiesReader('./config/config.properties');
let idmBaseUrl = properties.get('base.idm.url');
let apiBaseUrl = properties.get('base.api.url');
let idmUsername = properties.get('idm.username');
let idmPassword = properties.get('idm.password');
class serviceClient {
    /**
     * To get Access Token
     * @param endPoint
     * @param request
     */
    /*
    public async getAccessTokenService(endPoint,request){
        let url = idmBaseUrl+endPoint;
        var body = request.replace("idm.username",idmUsername).replace("idm.password",idmPassword);
        response = await RestUtilsobj.postformbody(url,body);
        const json = JSON.parse(response.body);
        console.log("STATUS CODE : "+response.statusCode);
        RestUtils.setAccessToken(json.access_token);
        RestUtils.setStatusCode(response.statusCode.toString());
    }
    */
    async getStatusCode() {
        return restUtils_1.RestUtils.getStatusCode().toString();
    }
    /**
     *  to get access token from the access token service
     * @param endPoint
     * @param request
     */
    async getAccessToken(endPoint, request) {
        let url = idmBaseUrl + endPoint;
        var body = request.replace("idm.username", idmUsername).replace("idm.password", idmPassword);
        const obj = JSON.parse(body);
        let headerMap = new Map();
        headerMap.set('Content-Type', 'application/x-www-form-urlencoded');
        response = await RestUtilsobj.doRequest(url, headerMap, obj, true, 'POST');
        const json = JSON.parse(response.body);
        restUtils_1.RestUtils.setAccessToken(json.access_token);
        restUtils_1.RestUtils.setStatusCode(response.statusCode.toString());
    }
    /**
     * to create user
     * @param request
     */
    async createUser(request) {
        let url = apiBaseUrl + "/users/v2/users";
        var accessToken = restUtils_1.RestUtils.getAccessToken();
        var headers = {
            Authorization: accessToken,
            'Content-Type': 'application/json'
        };
        response = await RestUtilsobj.doRequest(url, headers, request, false, 'POST');
        restUtils_1.RestUtils.setStatusCode(response.statusCode.toString());
    }
    /**
     *  To Get list of users
     */
    async getUserList() {
        let url = apiBaseUrl + "/users/v2/users";
        var accessToken = restUtils_1.RestUtils.getAccessToken();
        var headers = {
            Authorization: accessToken,
            'Content-Type': 'application/json'
        };
        response = await RestUtilsobj.doRequest(url, headers, '', false, 'GET');
        restUtils_1.RestUtils.setStatusCode(response.statusCode.toString());
    }
}
exports.serviceClient = serviceClient;
//# sourceMappingURL=serviceClient.js.map