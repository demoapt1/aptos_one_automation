"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Testrail = require('testrail-api');
let testrail = Testrail;
class IAddTestRunImpl {
    constructor() {
        this.assignedto_id = '';
        this.description = '';
        this.include_all = true;
        this.milestone_id = '';
        this.name = '';
        this.suite_id = '';
    }
}
exports.IAddTestRunImpl = IAddTestRunImpl;
class IContentObjectImpl {
    constructor() {
        this.assignedto_id = '';
        this.comment = '';
        this.defects = '';
        this.elapsed = '';
        this.status_id = 0;
        this.version = '';
    }
}
exports.IContentObjectImpl = IContentObjectImpl;
class TestRailreport {
    constructor() {
        testrail = new Testrail({
            host: 'https://testaptos.testrail.io',
            password: 'Aptos@1234',
            user: 'krishna01012002@gmail.com',
        });
    }
    addTestResult(runid, caseid, content) {
        // const contents = JSON.parse(j);
        /*  testrail.addResultForCase(1, 2, content, (err, response, result) => {
              console.log('RESULT ' + err + response, result);
          });*/
        content = new IContentObjectImpl();
        content.assignedto_id = '1';
        content.comment = 'update result';
        content.version = '100';
        content.defects = 'APT458';
        content.status_id = '5';
        testrail.addResultForCase(runid, caseid, content, (err, response, result) => {
            console.log('RESULT TTT' + err + response, result);
        });
    }
    createTestrun() {
        //  let isrunpresent: boolean = false;
        console.log('CREAT TEST RUN');
        testrail.addRun(/*PROJECT_ID=*/ 1, /*CONTENT=*/ {
            suite_id: '1',
            assignedto_id: '1',
            description: 'DESC REST_API_TEST_RUN_BUILD#100',
            name: 'REST_API_TEST_RUN_BUILD#100',
            include_all: true,
            case_ids: [],
            milestone_id: '1',
        }, (err, response, run) => {
            console.log(err + response + run);
        });
    }
}
exports.TestRailreport = TestRailreport;
//# sourceMappingURL=testrailUtil.js.map