Feature: Get User List


  Scenario: C3 Generating access token
    Given send POST request to "https://idm.dev.aptos-denim.aptos-labs.io/auth/realms/aptos-denim_default/protocol/openid-connect/token", input body is
    """
     {
    "grant_type":"password",
    "username":"swati.shikhar",
    "password":"Zensar@123",
    "client_id":"localhost-dev"
     }
    """
    Then the status code must be 200

  Scenario: C4 Get particular user from users list

    Given send GET request to "https://api.dev.aptos-denim.aptos-labs.io/users/v2/users/cdcd8598-46f2-4bec-8167-de3b800b0bde"

      Then user response includes the following
        | username                                                                 | 1111                 |
        | phoneNumber                                                              | 122-222-2222         |
        |securityRoleId                                                            | 10                   |
        |userId                                                                    | cdcd8598-46f2-4bec-8167-de3b800b0bde|

  Scenario: C5 Get users list with limit

    Given send GET request to "https://api.dev.aptos-denim.aptos-labs.io/users/v2/users?limit=5&offset=0"

    Then response includes the following
      | data[*].username                                                         | 1111,"testuser182436",1221                 |
      | data[*].phoneNumbers[*].phoneNumber                                      | 122-222-2222         |
      | data[*].retailLocationRoles[*].securityRoleId                            | 10                    |

    Then response not includes the following
      | data[5].username                                                         |   1226              |
