const Testrail = require('testrail-api');

let testrail = Testrail;

export interface IContentObject {
        assignedto_id: string;
        comment: string;
        defects: string;
        elapsed: string;
        status_id: number;
        version: string;
    }
export interface IAddTestRun {
    suite_id: string;
    assignedto_id: string;
    description: string;
    name: string;
    include_all: boolean;
    case_ids: any[];
    milestone_id: string;
}

export class IAddTestRunImpl implements IAddTestRun {
    public assignedto_id: string = '';
    public case_ids!: string[];
    public description: string = '';
    public include_all: boolean = true;
    public milestone_id: string = '';
    public name: string = '';
    public suite_id: string = '';
}

export class IContentObjectImpl implements IContentObject {
    public assignedto_id: string = '';
    public comment: string = '';
    public defects: string = '';
    public elapsed: string = '';
    public status_id: number = 0;
    public version: string = '';
}

export class TestRailreport {

    public constructor() {
    testrail = new Testrail({
        host: 'https://testaptos.testrail.io',
        password: 'Aptos@1234',
        user: 'krishna01012002@gmail.com',

    });
  }
 public  addTestResult(runid, caseid, content) {

      // const contents = JSON.parse(j);
      /*  testrail.addResultForCase(1, 2, content, (err, response, result) => {
            console.log('RESULT ' + err + response, result);
        });*/
    content = new IContentObjectImpl();
     content.assignedto_id = '1';
     content.comment = 'update result';

     content.version = '100';
     content.defects = 'APT458';
     content.status_id = '5';

         testrail.addResultForCase(runid, caseid, content, (err, response, result) => {
         console.log('RESULT TTT' + err + response, result);
     });
    }

    public createTestrun( ) {
      //  let isrunpresent: boolean = false;
        console.log('CREAT TEST RUN');
        testrail.addRun(/*PROJECT_ID=*/1, /*CONTENT=*/{
                   suite_id: '1',
                    assignedto_id: '1',
                    description: 'DESC REST_API_TEST_RUN_BUILD#100',
                    name: 'REST_API_TEST_RUN_BUILD#100',
                    include_all: true,
            case_ids: [],
            milestone_id: '1',
                },  (err, response, run) => {
                    console.log(err + response + run);
                });

    }
}
